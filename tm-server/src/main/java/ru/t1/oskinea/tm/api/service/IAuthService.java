package ru.t1.oskinea.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.model.User;

public interface IAuthService {

    @NotNull
    User check(@NotNull String login, @NotNull String password);

    @NotNull
    User registry(@NotNull String login, @NotNull String password, @NotNull String email);

}
