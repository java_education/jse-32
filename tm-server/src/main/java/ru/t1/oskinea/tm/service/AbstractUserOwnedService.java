package ru.t1.oskinea.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.oskinea.tm.api.repository.IUserOwnedRepository;
import ru.t1.oskinea.tm.api.service.IUserOwnedService;
import ru.t1.oskinea.tm.enumerated.Sort;
import ru.t1.oskinea.tm.exception.field.IdEmptyException;
import ru.t1.oskinea.tm.exception.field.IndexIncorrectException;
import ru.t1.oskinea.tm.exception.field.UserIdEmptyException;
import ru.t1.oskinea.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(final R repository) {
        super(repository);
    }

    @NotNull
    @Override
    public M add(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        return repository.add(userId, model);
    }

    @Override
    public void clear(@NotNull final String userId) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        repository.clear(userId);
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(userId, id);
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAll(userId);
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId, @Nullable final Comparator<M> comparator) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll(userId);
        return repository.findAll(userId, comparator);
    }

    @NotNull
    @Override
    @SuppressWarnings("unchecked")
    public List<M> findAll(@NotNull final String userId, @Nullable final Sort sort) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        return repository.findAll(userId, (Comparator<M>) sort.getComparator());
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(userId, id);
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (index < 0) throw new IndexIncorrectException();
        if (index >= getSize(userId)) throw new IndexIncorrectException();
        return repository.findOneByIndex(userId, index);
    }

    @Override
    public int getSize(@NotNull final String userId) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        return repository.getSize(userId);
    }

    @Nullable
    @Override
    public M remove(@NotNull final String userId, @Nullable final M model) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) return null;
        return repository.remove(userId, model);
    }

    @Nullable
    @Override
    public M removeById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        return repository.removeById(userId, id);
    }

    @Nullable
    @Override
    public M removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (index < 0) throw new IndexIncorrectException();
        if (index >= getSize(userId)) throw new IndexIncorrectException();
        return repository.removeByIndex(userId, index);
    }

}
