package ru.t1.oskinea.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.oskinea.tm.api.endpoint.ITaskEndpoint;
import ru.t1.oskinea.tm.api.service.IProjectTaskService;
import ru.t1.oskinea.tm.api.service.IServiceLocator;
import ru.t1.oskinea.tm.api.service.ITaskService;
import ru.t1.oskinea.tm.dto.request.task.*;
import ru.t1.oskinea.tm.dto.response.task.*;
import ru.t1.oskinea.tm.enumerated.Sort;
import ru.t1.oskinea.tm.enumerated.Status;
import ru.t1.oskinea.tm.model.Task;

import java.util.List;

public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    @NotNull
    private IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    @NotNull
    @Override
    public TaskBindToProjectResponse bindTaskToProject(@NotNull final TaskBindToProjectRequest request) {
        check(request);
        @NotNull final String projectId = request.getProjectId();
        @NotNull final String taskId = request.getTaskId();
        @NotNull final String userId = request.getUserId();
        @NotNull final Task task = getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
        return new TaskBindToProjectResponse(task);
    }

    @NotNull
    @Override
    public TaskChangeStatusByIdResponse changeTaskStatusById(@NotNull final TaskChangeStatusByIdRequest request) {
        check(request);
        @NotNull final String id = request.getId();
        @NotNull final Status status = request.getStatus();
        @NotNull final String userId = request.getUserId();
        @NotNull final Task task = getTaskService().changeTaskStatusById(userId, id, status);
        return new TaskChangeStatusByIdResponse(task);
    }

    @NotNull
    @Override
    public TaskChangeStatusByIndexResponse changeTaskStatusByIndex(@NotNull final TaskChangeStatusByIndexRequest request) {
        check(request);
        @NotNull final Integer index = request.getIndex();
        @NotNull final Status status = request.getStatus();
        @NotNull final String userId = request.getUserId();
        @NotNull final Task task = getTaskService().changeTaskStatusByIndex(userId, index, status);
        return new TaskChangeStatusByIndexResponse(task);
    }

    @NotNull
    @Override
    public TaskClearResponse clearTask(@NotNull final TaskClearRequest request) {
        check(request);
        @NotNull final String userId = request.getUserId();
        getTaskService().clear(userId);
        return new TaskClearResponse();
    }

    @NotNull
    @Override
    public TaskCompleteByIdResponse completeTaskById(@NotNull final TaskCompleteByIdRequest request) {
        check(request);
        @NotNull final String id = request.getId();
        @NotNull final String userId = request.getUserId();
        @NotNull final Task task = getTaskService().changeTaskStatusById(userId, id, Status.COMPLETED);
        return new TaskCompleteByIdResponse(task);
    }

    @NotNull
    @Override
    public TaskCompleteByIndexResponse completeTaskByIndex(@NotNull final TaskCompleteByIndexRequest request) {
        check(request);
        @NotNull final Integer index = request.getIndex();
        @NotNull final String userId = request.getUserId();
        @NotNull final Task task = getTaskService().changeTaskStatusByIndex(userId, index, Status.COMPLETED);
        return new TaskCompleteByIndexResponse(task);
    }

    @NotNull
    @Override
    public TaskCreateResponse createTask(@NotNull final TaskCreateRequest request) {
        check(request);
        @NotNull final String userId = request.getUserId();
        @NotNull final String name = request.getName();
        @NotNull final String description = request.getDescription();
        @NotNull final Task task = getTaskService().create(userId, name, description);
        return new TaskCreateResponse(task);
    }

    @NotNull
    @Override
    public TaskGetByIdResponse getTaskById(@NotNull final TaskGetByIdRequest request) {
        check(request);
        @NotNull final String id = request.getId();
        @NotNull final String userId = request.getUserId();
        @Nullable final Task task = getTaskService().findOneById(userId, id);
        return new TaskGetByIdResponse(task);
    }

    @NotNull
    @Override
    public TaskGetByIndexResponse getTaskByIndex(@NotNull final TaskGetByIndexRequest request) {
        check(request);
        @NotNull final Integer index = request.getIndex();
        @NotNull final String userId = request.getUserId();
        @Nullable final Task task = getTaskService().findOneByIndex(userId, index);
        return new TaskGetByIndexResponse(task);
    }

    @NotNull
    @Override
    public TaskGetByProjectIdResponse getTaskByProjectId(@NotNull final TaskGetByProjectIdRequest request) {
        check(request);
        @NotNull final String userId = request.getUserId();
        @NotNull final String projectId = request.getProjectId();
        @NotNull final List<Task> tasks = getTaskService().findAllByProjectId(userId, projectId);
        return new TaskGetByProjectIdResponse(tasks);
    }

    @NotNull
    @Override
    public TaskListResponse listTask(@NotNull final TaskListRequest request) {
        check(request);
        @NotNull final String userId = request.getUserId();
        @Nullable final Sort sort = request.getSort();
        @NotNull final List<Task> tasks = getTaskService().findAll(userId, sort);
        return new TaskListResponse(tasks);
    }

    @NotNull
    @Override
    public TaskRemoveByIdResponse removeTaskById(@NotNull final TaskRemoveByIdRequest request) {
        check(request);
        @NotNull final String id = request.getId();
        @NotNull final String userId = request.getUserId();
        @Nullable final Task task = getTaskService().removeById(userId, id);
        return new TaskRemoveByIdResponse(task);
    }

    @NotNull
    @Override
    public TaskRemoveByIndexResponse removeTaskByIndex(@NotNull final TaskRemoveByIndexRequest request) {
        check(request);
        @NotNull final Integer index = request.getIndex();
        @NotNull final String userId = request.getUserId();
        @Nullable final Task task = getTaskService().removeByIndex(userId, index);
        return new TaskRemoveByIndexResponse(task);
    }

    @NotNull
    @Override
    public TaskStartByIdResponse startTaskById(@NotNull final TaskStartByIdRequest request) {
        check(request);
        @NotNull final String id = request.getId();
        @NotNull final String userId = request.getUserId();
        @NotNull final Task task = getTaskService().changeTaskStatusById(userId, id, Status.IN_PROGRESS);
        return new TaskStartByIdResponse(task);
    }

    @NotNull
    @Override
    public TaskStartByIndexResponse startTaskByIndex(@NotNull final TaskStartByIndexRequest request) {
        check(request);
        @NotNull final Integer index = request.getIndex();
        @NotNull final String userId = request.getUserId();
        @NotNull final Task task = getTaskService().changeTaskStatusByIndex(userId, index, Status.IN_PROGRESS);
        return new TaskStartByIndexResponse(task);
    }

    @NotNull
    @Override
    public TaskUnbindFromProjectResponse unbindTaskFromProject(@NotNull final TaskUnbindFromProjectRequest request) {
        check(request);
        @NotNull final String projectId = request.getProjectId();
        @NotNull final String taskId = request.getTaskId();
        @NotNull final String userId = request.getUserId();
        @NotNull final Task task = getProjectTaskService().unbindTaskFromProject(userId, projectId, taskId);
        return new TaskUnbindFromProjectResponse(task);
    }

    @NotNull
    @Override
    public TaskUpdateByIdResponse updateTaskById(@NotNull final TaskUpdateByIdRequest request) {
        check(request);
        @NotNull final String id = request.getId();
        @NotNull final String userId = request.getUserId();
        @NotNull final String name = request.getName();
        @NotNull final String description = request.getDescription();
        @NotNull final Task task = getTaskService().updateById(userId, id, name, description);
        return new TaskUpdateByIdResponse(task);
    }

    @NotNull
    @Override
    public TaskUpdateByIndexResponse updateTaskByIndex(@NotNull final TaskUpdateByIndexRequest request) {
        check(request);
        @NotNull final Integer index = request.getIndex();
        @NotNull final String userId = request.getUserId();
        @NotNull final String name = request.getName();
        @NotNull final String description = request.getDescription();
        @NotNull final Task task = getTaskService().updateByIndex(userId, index, name, description);
        return new TaskUpdateByIndexResponse(task);
    }

}
