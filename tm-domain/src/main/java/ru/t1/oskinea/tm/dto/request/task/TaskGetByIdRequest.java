package ru.t1.oskinea.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskGetByIdRequest extends AbstractUserRequest {

    @NotNull
    private String id;

    public TaskGetByIdRequest(@NotNull final String id) {
        this.id = id;
    }

}
