package ru.t1.oskinea.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.dto.request.AbstractUserRequest;
import ru.t1.oskinea.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectChangeStatusByIndexRequest extends AbstractUserRequest {

    @NotNull
    private Integer index;

    @NotNull
    private Status status;

    public ProjectChangeStatusByIndexRequest(@NotNull final Integer index, @NotNull final Status status) {
        this.index = index;
        this.status = status;
    }

}
