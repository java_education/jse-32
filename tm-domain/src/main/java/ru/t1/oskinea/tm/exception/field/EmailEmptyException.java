package ru.t1.oskinea.tm.exception.field;

public final class EmailEmptyException extends AbstractFieldException {

    public EmailEmptyException() {
        super("Error! E-mail is empty...");
    }

}
