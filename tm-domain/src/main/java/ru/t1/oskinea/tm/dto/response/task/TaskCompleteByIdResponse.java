package ru.t1.oskinea.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.dto.response.AbstractResponse;
import ru.t1.oskinea.tm.model.Task;

@Getter
@Setter
@NoArgsConstructor
public final class TaskCompleteByIdResponse extends AbstractResponse {

    @NotNull
    private Task task;

    public TaskCompleteByIdResponse(@NotNull final Task task) {
        this.task = task;
    }

}
