package ru.t1.oskinea.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.dto.response.AbstractResponse;
import ru.t1.oskinea.tm.model.User;

@Getter
@Setter
@NoArgsConstructor
public final class UserUpdateProfileResponse extends AbstractResponse {

    @NotNull
    private User user;

    public UserUpdateProfileResponse(@NotNull final User user) {
        this.user = user;
    }

}
