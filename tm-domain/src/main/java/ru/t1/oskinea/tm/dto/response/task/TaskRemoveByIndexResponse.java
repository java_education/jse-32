package ru.t1.oskinea.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.oskinea.tm.dto.response.AbstractResponse;
import ru.t1.oskinea.tm.model.Task;

@Getter
@Setter
@NoArgsConstructor
public final class TaskRemoveByIndexResponse extends AbstractResponse {

    @Nullable
    private Task task;

    public TaskRemoveByIndexResponse(@Nullable final Task task) {
        this.task = task;
    }

}
