package ru.t1.oskinea.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskCreateRequest extends AbstractUserRequest {

    @NotNull
    private String name;

    @NotNull
    private String description;

    public TaskCreateRequest(@NotNull final String name, @NotNull final String description) {
        this.name = name;
        this.description = description;
    }

}
