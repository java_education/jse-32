package ru.t1.oskinea.tm.exception.field;

public final class StatusIncorrectException extends AbstractFieldException {

    public StatusIncorrectException() {
        super("Error! Status incorrect...");
    }

}
