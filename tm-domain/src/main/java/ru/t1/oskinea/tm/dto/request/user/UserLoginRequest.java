package ru.t1.oskinea.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class UserLoginRequest extends AbstractUserRequest {

    @NotNull
    private String login;

    @NotNull
    private String password;

    public UserLoginRequest(@NotNull final String login, @NotNull final String password) {
        this.login = login;
        this.password = password;
    }

}
