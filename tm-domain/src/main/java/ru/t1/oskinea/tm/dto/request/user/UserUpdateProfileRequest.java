package ru.t1.oskinea.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class UserUpdateProfileRequest extends AbstractUserRequest {

    @NotNull
    private String firstName;

    @NotNull
    private String middleName;

    @NotNull
    private String lastName;

    public UserUpdateProfileRequest(
            @NotNull final String firstName,
            @NotNull final String middleName,
            @NotNull final String lastName
    ) {
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
    }

}
