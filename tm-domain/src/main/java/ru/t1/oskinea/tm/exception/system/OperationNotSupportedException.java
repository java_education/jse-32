package ru.t1.oskinea.tm.exception.system;

import org.jetbrains.annotations.NotNull;

public final class OperationNotSupportedException extends AbstractSystemException {

    @SuppressWarnings("unused")
    public OperationNotSupportedException() {
        super("Error! Operation is not supported...");
    }

    public OperationNotSupportedException(@NotNull final String operation) {
        super("Error! Operation ``" + operation + "`` is not supported...");
    }

}
