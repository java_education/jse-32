package ru.t1.oskinea.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.dto.request.AbstractUserRequest;
import ru.t1.oskinea.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public final class TaskChangeStatusByIdRequest extends AbstractUserRequest {

    @NotNull
    private String id;

    @NotNull
    private Status status;

    public TaskChangeStatusByIdRequest(@NotNull final String id, @NotNull final Status status) {
        this.id = id;
        this.status = status;
    }

}
