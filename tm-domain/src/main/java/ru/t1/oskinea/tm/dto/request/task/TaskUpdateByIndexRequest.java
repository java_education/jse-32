package ru.t1.oskinea.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskUpdateByIndexRequest extends AbstractUserRequest {

    @NotNull
    private Integer index;

    @NotNull
    private String name;

    @NotNull
    private String description;

    public TaskUpdateByIndexRequest(
            @NotNull final Integer index,
            @NotNull final String name,
            @NotNull final String description
    ) {
        this.index = index;
        this.name = name;
        this.description = description;
    }

}
