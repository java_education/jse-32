package ru.t1.oskinea.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class UserChangePasswordRequest extends AbstractUserRequest {

    @NotNull
    private String password;

    public UserChangePasswordRequest(@NotNull final String password) {
        this.password = password;
    }

}
