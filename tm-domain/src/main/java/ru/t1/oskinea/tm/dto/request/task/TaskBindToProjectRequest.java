package ru.t1.oskinea.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskBindToProjectRequest extends AbstractUserRequest {

    @NotNull
    private String taskId;

    @NotNull
    private String projectId;

    public TaskBindToProjectRequest(@NotNull final String taskId, @NotNull final String projectId) {
        this.taskId = taskId;
        this.projectId = projectId;
    }

}
