package ru.t1.oskinea.tm.exception;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public abstract class AbstractException extends RuntimeException {

    public AbstractException(@NotNull final String message) {
        super(message);
    }

    public AbstractException(@NotNull final String message, @Nullable final Throwable cause) {
        super(message, cause);
    }

    public AbstractException(@Nullable final Throwable cause) {
        super(cause);
    }

    public AbstractException(
            @NotNull final String message,
            @Nullable final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
