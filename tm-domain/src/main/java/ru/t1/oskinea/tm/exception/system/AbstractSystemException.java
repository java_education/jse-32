package ru.t1.oskinea.tm.exception.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.oskinea.tm.exception.AbstractException;

@NoArgsConstructor
public abstract class AbstractSystemException extends AbstractException {

    public AbstractSystemException(@NotNull final String message) {
        super(message);
    }

    @SuppressWarnings("unused")
    public AbstractSystemException(@NotNull final String message, @Nullable final Throwable cause) {
        super(message, cause);
    }

    @SuppressWarnings("unused")
    public AbstractSystemException(@Nullable final Throwable cause) {
        super(cause);
    }

    @SuppressWarnings("unused")
    public AbstractSystemException(
            @NotNull final String message,
            @Nullable final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
