package ru.t1.oskinea.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.oskinea.tm.api.endpoint.IUserEndpointClient;
import ru.t1.oskinea.tm.command.AbstractCommand;
import ru.t1.oskinea.tm.exception.entity.UserNotFoundException;
import ru.t1.oskinea.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected void showUser(@Nullable final User user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
    }

    @NotNull
    public IUserEndpointClient getUserEndpoint() {
        return serviceLocator.getUserEndpointClient();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

}
