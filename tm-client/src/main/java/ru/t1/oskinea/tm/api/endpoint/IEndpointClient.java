package ru.t1.oskinea.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.net.Socket;

public interface IEndpointClient {

    @Nullable
    Socket connect() throws IOException;

    void disconnect() throws IOException;

    void setSocket(@NotNull Socket socket);

}
