package ru.t1.oskinea.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.oskinea.tm.dto.request.project.ProjectListRequest;
import ru.t1.oskinea.tm.dto.response.project.ProjectListResponse;
import ru.t1.oskinea.tm.enumerated.Sort;
import ru.t1.oskinea.tm.model.Project;
import ru.t1.oskinea.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    @NotNull
    private static final String DESCRIPTION = "Show project list.";

    @NotNull
    private static final String NAME = "project-list";

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECTS]");
        System.out.print("ENTER SORT (");
        System.out.print(Arrays.toString(Sort.values()));
        System.out.print("): ");
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);

        @NotNull final ProjectListRequest request = new ProjectListRequest();
        if (sort != null) request.setSort(sort);
        @Nullable final ProjectListResponse response = getProjectEndpoint().listProject(request);

        @NotNull final List<Project> projects = response.getProjects();
        int index = 1;
        for (@Nullable final Project project : projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project.getName());
            index++;
        }
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
