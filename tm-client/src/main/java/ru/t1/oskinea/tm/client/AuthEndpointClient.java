package ru.t1.oskinea.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.api.endpoint.IAuthEndpointClient;
import ru.t1.oskinea.tm.dto.request.user.UserGetProfileRequest;
import ru.t1.oskinea.tm.dto.request.user.UserLoginRequest;
import ru.t1.oskinea.tm.dto.request.user.UserLogoutRequest;
import ru.t1.oskinea.tm.dto.response.user.UserGetProfileResponse;
import ru.t1.oskinea.tm.dto.response.user.UserLoginResponse;
import ru.t1.oskinea.tm.dto.response.user.UserLogoutResponse;

@NoArgsConstructor
public final class AuthEndpointClient extends AbstractEndpointClient implements IAuthEndpointClient {

    public AuthEndpointClient(@NotNull final AuthEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLoginResponse login(@NotNull final UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLogoutResponse logout(@NotNull final UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserGetProfileResponse profile(@NotNull final UserGetProfileRequest request) {
        return call(request, UserGetProfileResponse.class);
    }

    @SneakyThrows
    public static void main(final String[] args) {
        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        System.out.println(authEndpointClient.profile(new UserGetProfileRequest()).getUser());

        System.out.println(authEndpointClient.login(new UserLoginRequest("test_invalid", "test")).getSuccess());
        System.out.println(authEndpointClient.profile(new UserGetProfileRequest()).getUser());

        System.out.println(authEndpointClient.login(new UserLoginRequest("test", "test")).getSuccess());
        System.out.println(authEndpointClient.profile(new UserGetProfileRequest()).getUser().getEmail());
        System.out.println(authEndpointClient.logout(new UserLogoutRequest()));

        System.out.println(authEndpointClient.profile(new UserGetProfileRequest()).getUser());
        authEndpointClient.disconnect();
    }

}
