package ru.t1.oskinea.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.api.endpoint.ITaskEndpointClient;
import ru.t1.oskinea.tm.dto.request.task.*;
import ru.t1.oskinea.tm.dto.request.user.UserGetProfileRequest;
import ru.t1.oskinea.tm.dto.request.user.UserLoginRequest;
import ru.t1.oskinea.tm.dto.request.user.UserLogoutRequest;
import ru.t1.oskinea.tm.dto.response.task.*;

@NoArgsConstructor
public final class TaskEndpointClient extends AbstractEndpointClient implements ITaskEndpointClient {

    public TaskEndpointClient(@NotNull final AbstractEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    public TaskBindToProjectResponse bindTaskToProject(@NotNull final TaskBindToProjectRequest request) {
        return call(request, TaskBindToProjectResponse.class);
    }

    @NotNull
    @Override
    public TaskChangeStatusByIdResponse changeTaskStatusById(@NotNull final TaskChangeStatusByIdRequest request) {
        return call(request, TaskChangeStatusByIdResponse.class);
    }

    @NotNull
    @Override
    public TaskChangeStatusByIndexResponse changeTaskStatusByIndex(@NotNull final TaskChangeStatusByIndexRequest request) {
        return call(request, TaskChangeStatusByIndexResponse.class);
    }

    @NotNull
    @Override
    public TaskClearResponse clearTask(@NotNull final TaskClearRequest request) {
        return call(request, TaskClearResponse.class);
    }

    @NotNull
    @Override
    public TaskCompleteByIdResponse completeTaskById(@NotNull final TaskCompleteByIdRequest request) {
        return call(request, TaskCompleteByIdResponse.class);
    }

    @NotNull
    @Override
    public TaskCompleteByIndexResponse completeTaskByIndex(@NotNull final TaskCompleteByIndexRequest request) {
        return call(request, TaskCompleteByIndexResponse.class);
    }

    @NotNull
    @Override
    public TaskCreateResponse createTask(@NotNull final TaskCreateRequest request) {
        return call(request, TaskCreateResponse.class);
    }

    @NotNull
    @Override
    public TaskGetByIdResponse getTaskById(@NotNull final TaskGetByIdRequest request) {
        return call(request, TaskGetByIdResponse.class);
    }

    @NotNull
    @Override
    public TaskGetByIndexResponse getTaskByIndex(@NotNull final TaskGetByIndexRequest request) {
        return call(request, TaskGetByIndexResponse.class);
    }

    @NotNull
    @Override
    public TaskGetByProjectIdResponse getTaskByProjectId(@NotNull final TaskGetByProjectIdRequest request) {
        return call(request, TaskGetByProjectIdResponse.class);
    }

    @NotNull
    @Override
    public TaskListResponse listTask(@NotNull final TaskListRequest request) {
        return call(request, TaskListResponse.class);
    }

    @NotNull
    @Override
    public TaskRemoveByIdResponse removeTaskById(@NotNull final TaskRemoveByIdRequest request) {
        return call(request, TaskRemoveByIdResponse.class);
    }

    @NotNull
    @Override
    public TaskRemoveByIndexResponse removeTaskByIndex(@NotNull final TaskRemoveByIndexRequest request) {
        return call(request, TaskRemoveByIndexResponse.class);
    }

    @NotNull
    @Override
    public TaskStartByIdResponse startTaskById(@NotNull final TaskStartByIdRequest request) {
        return call(request, TaskStartByIdResponse.class);
    }

    @NotNull
    @Override
    public TaskStartByIndexResponse startTaskByIndex(@NotNull final TaskStartByIndexRequest request) {
        return call(request, TaskStartByIndexResponse.class);
    }

    @NotNull
    @Override
    public TaskUnbindFromProjectResponse unbindTaskFromProject(@NotNull final TaskUnbindFromProjectRequest request) {
        return call(request, TaskUnbindFromProjectResponse.class);
    }

    @NotNull
    @Override
    public TaskUpdateByIdResponse updateTaskById(@NotNull final TaskUpdateByIdRequest request) {
        return call(request, TaskUpdateByIdResponse.class);
    }

    @NotNull
    @Override
    public TaskUpdateByIndexResponse updateTaskByIndex(@NotNull final TaskUpdateByIndexRequest request) {
        return call(request, TaskUpdateByIndexResponse.class);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        System.out.println(authEndpointClient.login(new UserLoginRequest("test", "test")).getSuccess());
        System.out.println(authEndpointClient.profile(new UserGetProfileRequest()).getUser().getEmail());

        @NotNull final TaskEndpointClient taskEndpointClient = new TaskEndpointClient(authEndpointClient);
        System.out.println(taskEndpointClient.createTask(new TaskCreateRequest("tsk_new", "desc_new")).getTask());
        System.out.println(taskEndpointClient.listTask(new TaskListRequest()).getTasks());

        System.out.println(authEndpointClient.logout(new UserLogoutRequest()));
        authEndpointClient.disconnect();
    }

}
