package ru.t1.oskinea.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.oskinea.tm.api.endpoint.ISystemEndpointClient;
import ru.t1.oskinea.tm.dto.request.system.ServerAboutRequest;
import ru.t1.oskinea.tm.dto.request.system.ServerVersionRequest;
import ru.t1.oskinea.tm.dto.response.system.ServerAboutResponse;
import ru.t1.oskinea.tm.dto.response.system.ServerVersionResponse;

@NoArgsConstructor
public final class SystemEndpointClient extends AbstractEndpointClient implements ISystemEndpointClient {

    @NotNull
    @Override
    @SneakyThrows
    public ServerAboutResponse getAbout(@NotNull final ServerAboutRequest request) {
        return (ServerAboutResponse) call(request);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerVersionResponse getVersion(@NotNull final ServerVersionRequest request) {
        return (ServerVersionResponse) call(request);
    }

    @SneakyThrows
    public static void main(@Nullable final String[] args) {
        @NotNull final SystemEndpointClient client = new SystemEndpointClient();
        client.connect();

        @NotNull final ServerAboutResponse serverAboutResponse = client.getAbout(new ServerAboutRequest());
        System.out.println(serverAboutResponse.getName());
        System.out.println(serverAboutResponse.getEmail());

        @NotNull final ServerVersionResponse serverVersionResponse = client.getVersion(new ServerVersionRequest());
        System.out.println(serverVersionResponse.getVersion());

        client.disconnect();
    }

}
