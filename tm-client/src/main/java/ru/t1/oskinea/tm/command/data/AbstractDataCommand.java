package ru.t1.oskinea.tm.command.data;

import ru.t1.oskinea.tm.api.endpoint.IDomainEndpointClient;
import ru.t1.oskinea.tm.command.AbstractCommand;

public abstract class AbstractDataCommand extends AbstractCommand {

    public AbstractDataCommand() {
    }

    public IDomainEndpointClient getDomainEndpoint() {
        return getServiceLocator().getDomainEndpointClient();
    }

}
