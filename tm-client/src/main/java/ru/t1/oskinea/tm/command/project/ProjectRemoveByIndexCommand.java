package ru.t1.oskinea.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.dto.request.project.ProjectRemoveByIndexRequest;
import ru.t1.oskinea.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private static final String DESCRIPTION = "Remove project by index.";

    @NotNull
    private static final String NAME = "project-remove-by-index";

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.print("ENTER INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;

        @NotNull final ProjectRemoveByIndexRequest request = new ProjectRemoveByIndexRequest();
        request.setIndex(index);

        getProjectEndpoint().removeProjectByIndex(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
