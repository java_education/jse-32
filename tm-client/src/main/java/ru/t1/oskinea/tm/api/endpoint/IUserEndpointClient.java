package ru.t1.oskinea.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.oskinea.tm.dto.request.user.UserGetProfileRequest;
import ru.t1.oskinea.tm.dto.request.user.UserLoginRequest;
import ru.t1.oskinea.tm.dto.request.user.UserLogoutRequest;
import ru.t1.oskinea.tm.dto.response.user.UserGetProfileResponse;
import ru.t1.oskinea.tm.dto.response.user.UserLoginResponse;
import ru.t1.oskinea.tm.dto.response.user.UserLogoutResponse;

public interface IUserEndpointClient extends IUserEndpoint, IEndpointClient {

    @NotNull
    UserGetProfileResponse getUserProfile(@NotNull UserGetProfileRequest request);

    @NotNull
    UserLoginResponse loginUser(@NotNull UserLoginRequest request);

    @NotNull
    UserLogoutResponse logoutUser(@NotNull UserLogoutRequest request);

}
